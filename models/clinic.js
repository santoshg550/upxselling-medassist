const mongoose = require('mongoose')
const Schema = mongoose.Schema

const clinicSchema = new Schema({
    name: String,
    mobNo: {
        type: Number,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    address: {
        line_1: String,
        line_2: String,
        country: String,
        city: String,
        state: String,
        pincode: Number
    },
    logo: Buffer,
    resetPasswordToken: String,
    resetPasswordSalt: String,
    resetPasswordExpires: Date,
    lastLogin: Date,
    updatedOn: Date,
    isVerified: Boolean
})

clinicSchema.pre('save', function(next) {
    this.updatedOn = new Date

    next()
})

const tokenSchema = new Schema({
    userId: {
        type: String,
        required: true
    },
    token: {
        type: String, required: true
    },
    createdAt: {
        type: Date, required: true, default: Date.now, expires: 86400
    }
})

module.exports = {
    Clinic: mongoose.model('Clinic', clinicSchema),
    Token: mongoose.model('Token', tokenSchema)
}
