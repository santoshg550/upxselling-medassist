const mongoose = require('mongoose')

const Schema = mongoose.Schema

const counterSchema = new Schema({
    clinicId: String,
    count: Number
})

counterSchema.statics.getCount = function(clinicId, cb) {
    this.findOneAndUpdate({ clinicId: clinicId }, {  $inc: { count: 1 } }, { new: true }, (err, doc) => {
        if(err)
            return cb(err)

        cb(null, doc)
    })
}

const Counter = mongoose.model('Counter', counterSchema)

module.exports = Counter
