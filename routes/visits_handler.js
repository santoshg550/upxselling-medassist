const mongoose = require('mongoose')

const Visit = require('../models/visit')
const Medicine = require('../models/medicine')
const log = require('../lib/log')
const val = require('./validations').visit

function validateVisit(req, res, next) {
    const visit = req.body

    if(!val.docId(visit.docId))
        return buildError('Doctor id is wrong')

    if(!val.patId(visit.patId))
        return buildError('Patient id is wrong')

    if(!val.patName(visit.patName))
        return buildError('Patient name is wrong')

    if(!val.docName(visit.docName))
        return buildError('Doctor name is wrong')

    if(!val.reason(visit.reason))
        return buildError('Reason is wrong')

    if(!val.bill(visit.bill))
        return buildError('Bill is wrong')

    if(!val.paid(visit.paid, visit.bill))
        return buildError('Paid is wrong')

    if(!val.presc(visit.prescription))
        return buildError('Prescription is wrong')

    if(!val.visitDate(visit.visitDate))
        return buildError('Visit date is wrong')

    const error = val.medicines(visit.medicines, visit.prescription)
    if(error)
        return error

    if(visit.medicines)
        return validateMeds(req, res, (err) => {
            if(err)
                return

            next()
        })

    next()
}

function validateMeds(req, res, callback) {
    const medicines = req.body.medicines

    const meds = []
    for(let i = 0; i < medicines.length; i++)
        try {
            meds.push(mongoose.Types.ObjectId(medicines[i]))
        } catch(err) {
            res.error(400, buildError({
                medId: medicines[i],
                message: 'Medicine id is wrong'
            }))

            return callback(true)
        }

    Medicine.find({ _id: { $in: meds }}, (err, docs) => {
        if(err) {
            res.error(500, errors.dbError(err).message)
            return callback(true)
        }

        if(!docs.length) {
            res.error(404, 'No such medicines')
            return callback(true) 
        }

        const medIds = []

        for(let j = 0; j < docs.length; j++)
            medIds.push(docs[j].id)

        for(let i = 0; i < medicines.length; i++)
            if(medIds.indexOf(medicines[i]) == -1) {
                res.error(404, medicines[i] + ' does not exist')
                return callback(true)
            }

        callback()
    })
}

function add(req, res) {
    const reqBody = req.body

    Visit.findOne({
        clinicId: req.session._id,
        docId: reqBody.docId,
        patId: reqBody.patId,
        patName: reqBody.patName,
        docName: reqBody.docName,
        reason: reqBody.reason,
        bill: reqBody.bill,
        paid: reqBody.paid,
        prescription: reqBody.prescription,
        visitDate: reqBody.visitDate
    }, (err, visit) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(visit)
            return res.error(409, 'Duplicate visit')

        visit = new Visit({
            clinicId: req.session._id,
            docId: reqBody.docId,
            patId: reqBody.patId,
            patName: reqBody.patName,
            docName: reqBody.docName,
            reason: reqBody.reason,
            bill: reqBody.bill,
            paid: reqBody.paid,
            visitDate: reqBody.visitDate,
            prescription: reqBody.prescription,
            prescImg: reqBody.prescImg ? Buffer(reqBody.prescImg, 'base64') : undefined,
            medicines: [...new Set(reqBody.medicines)]
        })

        visit.save((err) => {
            if(err)
                return res.error(500, errors.dbError(err).message)

            res.end('Record added')
        })
    })
}

function validateUpdate(req, res, next) {
    const update = req.body

    if(!val.id(update._id))
        return buildError('Visit id is wrong')

    return validateVisit(req, res, next)
}

function updateVisit(req, res) {
    const update = req.body

    Visit.findOne({ _id: update._id }, (err, visit) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(!visit)
            return res.error(400, 'No such visit')

        visit.docId = update.docId

        visit.patId = update.patId

        visit.patName = update.patName

        visit.docName = update.docName

        visit.reason = update.reason

        visit.bill = update.bill

        visit.paid = update.paid

        visit.visitDate = update.visitDate

        visit.prescription = update.prescription

        visit.prescImg = update.prescImg ? Buffer(update.prescImg, 'base64') : visit.prescImg

        visit.medicines = [...new Set(update.medicines)]

        visit.save((err) => {
            if(err)
                return res.error(500, errors.dbError(err).message)

            res.end('Updated')
        })
    })
}

function validateView(view) {
    if(!val.view(view))
        return buildError('Please provide a date or (from, to) or doctor id')
}

function view(req, res) {
    const reqBody = req.body

    let query = {}

    if(reqBody.date)
        query.visitDate = reqBody.date
    else if(reqBody.from)
        query.visitDate = {
            $gte: reqBody.from,
            $lte: reqBody.to
        }
    else if(reqBody.docId)
        query.docId = reqBody.docId

    Visit.find(query).populate('medicines').exec((err, visits) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(!visits.length)
            return res.error(404, 'No such visit')

        res.json(visits)
    })
}

module.exports = {
    validateVisit: validateVisit,
    add: add,
    validateUpdate: validateUpdate,
    updateVisit: updateVisit,
    validateView: validateView,
    view: view,
    validateMeds: validateMeds
}
