const validator = require('validator')

const { checkValue, checkString, checkNumber, validPassword, checkDate } = require('../lib/validation')

const onlyNumbers = /^\d+$/

function isAddressValid(address) {
    if(address.line_1 !== undefined && !checkString(address.line_1))
        return false

    if(address.line_2 !== undefined && !checkString(address.line_2))
        return false

    if(address.pincode !== undefined && !checkNumber(address.pincode))
        return false

    return checkValue(
        address.country, 'String',
        address.state, 'String',
        address.city, 'String'
    )
}

module.exports = {
    medicine: {
        isQueryValid: (query) => {
            if(!query)
                return false

            if(query.trim)
                query = query.trim()

            if(checkString(query) && query.length >= 3)
                return true

            return false
        }
    },

    report: {
        period: (range) => {
            if(!isNaN(Number(range.from)) || !checkDate(range.from))
                return buildError('from param is wrong')

            if(!isNaN(Number(range.to)) || !checkDate(range.to))
                return buildError('to param is wrong')

            if(new Date(range.from) > new Date(range.to))
                return buildError('From can not be after to')
        }
    },

    visit: {
        id: checkString,

        docId: checkString,

        patId: checkNumber,

        patName: checkString,

        docName: checkString,

        reason: checkString,

        bill: (bill) => {
            return bill !== undefined && checkNumber(bill)
        },

        paid: (paid, bill) => {
            return paid != undefined && checkNumber(paid) && paid <= bill
        },

        visitDate: (date) => {
            if(!isNaN(Number(date)) || !checkValue(date, 'Date'))
                return false

            if(Date.now() <= new Date(date).getTime())
                return false

            return true
        },

        presc: (presc) => {
            return presc == undefined || checkString(presc)
        },

        view: (options) => {
            if(options.date != undefined && isNaN(options.date) && checkValue(options.date, 'Date'))
                return true

            if(options.from != undefined && options.to != undefined) {
                if(!isNaN(options.from) || !isNaN(options.to))
                    return false

                if(checkValue(options.from, 'Date') && checkValue(options.to, 'Date'))
                    return true
            }

            if(options.docId != undefined && isNaN(options.docId) && checkString(options.docId))
                return true

            return false
        },

        medicines: (meds, presc) => {
            if(meds == undefined)
                return

            if(meds !== undefined && !presc)
                return buildError('Medicines are given without prescription. How ?')

            if(!checkValue(meds, 'Array'))
                return buildError('Medicines should be an array with non zero length')

            for(let i = 0; i < meds.length; i++)
                if(!checkString(meds[i]))
                    return buildError('One of the medicine id is wrong')
        }
    },

    doctor: {
        isNameValid: checkString,

        isEmailValid: (email) => {
            return checkString(email) && validator.isEmail(email)
        },

        isMobValid: (mob) => {
            return validator.isMobilePhone(String(mob), 'en-IN') && checkNumber(mob)
        },

        isAddressValid: isAddressValid,

        isDegValid: (degrees = []) => {
            for(let i = 0; i < degrees.length; i++) {
                if(!checkString(degrees[i].degree))
                    return buildError('Degree is wrong')

                if(!checkNumber(degrees[i].compYear))
                    return buildError('Completion year is wrong')

                if(!checkString(degrees[i].university))
                    return buildError('University is wrong')

                if(!checkString(degrees[i].college))
                    return buildError('College is wrong')
            }
        },

        isSpecValid: (specialties = []) => {
            for(let i = 0; i < specialties.length; i++) {
                if(!checkString(specialties[i].specialty))
                    return buildError('Specialty is wrong')

                if(!checkNumber(specialties[i].experience))
                    return buildError('Experience is wrong')

                if(!checkString(specialties[i].residency))
                    return buildError('Residency is wrong')

                if(!checkString(specialties[i].authority))
                    return buildError('Authority is wrong')
            }
        }
    },

    patient: {
        isIdValid: checkNumber,

        isFNValid: checkString,
        isLNValid: checkString,

        isEmailValid: (email) => {
            return !email || checkString(email) && validator.isEmail(email)
        },

        isAgeValid: (age) => {
            return !age || checkNumber(age)
        },

        isMobValid: (mob) => {
            return !mob || checkNumber(mob) && validator.isMobilePhone(String(mob), 'en-IN')
        },

        isGenderValid: (gender) => {
            return checkString(gender) && ['M', 'F', 'T'].includes(gender)
        },

        isAddressValid: isAddressValid,

        isFromToValid: (from, to) => {
            from = Number(from)
            to = Number(to)

            if(from == NaN || to == NaN)
                return false

            if(!onlyNumbers.test(from) || !onlyNumbers.test(to))
                return false

            if(to < from)
                return false

            return true
        },

        isQueryValid: (query) => {
            if(!query)
                return false

            if(query.trim)
                query = query.trim()

            if(checkString(query) && query.length >= 3)
                return true

            return false
        }
    },

    clinic: {
        isNameValid: checkString,

        isEmailValid: (email) => {
            return checkString(email) && validator.isEmail(email)
        },

        isMobValid: (mob) => {
            return validator.isMobilePhone(String(mob), 'en-IN') && checkNumber(mob)
        },

        isAddressValid: isAddressValid,

        isPasswordValid: (password) => {
            return validPassword.test(password)
        },

        isLogoValid: (logo) => {
            return Buffer.from(logo, 'base64').length <= 51200
        }
    }
}
