const express = require('express')

const handler = require('./patients_handler.js')

const router = express.Router()

router.use('/', (req, res, next) => {
    const error = handler.validateView(req.body)

    if(error)
        return res.error(400, error)

    next()
})

router.post('/', handler.view)

router.use('/register', (req, res, next) => {
    const error = handler.validateReg(req.body)

    if(error)
        return res.error(400, error)

    next()
})

router.post('/register', handler.register)

router.use('/update', (req, res, next) => {
    const error = handler.validateUpdate(req.body)

    if(error)
        return res.error(400, error)

    next()
})

router.post('/update', handler.updateReg)

router.use('/search', (req, res, next) => {
    const error = handler.validateSearch(req.body)

    if(error)
        return res.error(400, error)

    next()
})

router.post('/search', handler.search)

module.exports = router
