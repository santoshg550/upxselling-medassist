const mongoose = require('mongoose')

const log = require('../lib/log')
const Visit = require('../models/visit')
const val = require('./validations').report

function validateRange(range) {
    const error = val.period(range)

    if(error)
        return error
}

function income(req, res) {
    Visit.aggregate([
        {
            $match: {
                clinicId: req.session._id,
                visitDate: {
                    $gte: new Date(req.body.from),
                    $lte: new Date(req.body.to)
                }
            }
        },
        {
            $group: {
                _id: "$clinicId",
                income: { $sum: "$bill" }
            }
        }
    ], (err, result) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(result.length == 0)
            return res.status(204).end()

        res.json(result)
    })
}

function visitors(req, res) {
    Visit.aggregate([
        {
            $match: {
                clinicId: req.session._id,
                visitDate: {
                    $gte: new Date(req.body.from),
                    $lte: new Date(req.body.to)
                }
            }
        },
        {
            $count: 'totVisitors'
        }
    ], (err, result) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(result.length == 0)
            return res.status(204).end()

        res.json(result)
    })
}

module.exports = {
    validateRange: validateRange,
    income: income,
    visitors: visitors
}
