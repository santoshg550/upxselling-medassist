#!/usr/bin/env node

const http = require('http')
const fs = require('fs')
const blocked = require('blocked')

const Connector = require('./connections')
const config = require('./config')
const log = require('./lib/log')

errors = require('./lib/errors')
buildError = errors.buildError

const app = require('./app')

blocked((ms) => {
    log.info('BLOCKED FOR ' + (ms | 0) + 'ms')
})

function begin(callback) {
    log.info('Waiting for connections...')

    let server

    function createServer() {
        log.info('Services connected')

        server = http.createServer(app)

        server.on('error', shutdown)

        server.listen(config.port, function() {
            log.info('Server is listening on port ' + this.address().port);

            if(callback)
                callback(app)
        })
    }

    function shutdown(err, p) {
        if(err)
            log.error({ err: err, promise: p ? true : undefined }, 'Server error')

        connector.disconnect((err) => {
            if(err)
                log.error({ err: err }, 'Error disconnecting services')

            if(server)
                server.close(() => {
                    log.info('Server is shutting down')

                    setTimeout(() => {
                        process.exit()
                    }, 2000)
                })
        })
    }

    function onDisconnect() {
        log.fatal('Disconnected from services')
    }

    process.on('SIGTERM', () => {
        log.info('SIGTERM received')
        shutdown()
    })

    process.on('SIGINT', () => {
        log.info('SIGINT received')
        shutdown()
    })

    process.on('uncaughtException', shutdown)
    process.on('unhandledRejection', shutdown)

    const connector = new Connector

    connector
        .on('error', shutdown)
        .on('connected', createServer)
        .on('disconnected', onDisconnect)

    connector.connect()
}

if(require.main == module)
    begin()
else
    module.exports = begin
