var crypto = require('crypto');

var log = require('./log');

module.exports = {
    genRandomString: (length, byteToStringEncoding, callback) => {
        length = Number(length)

        if(isNaN(length))
            return callback(buildError('Length passed is not numeric'))

        crypto.randomBytes(Math.ceil(length/2), (err, buf) => {
            if(err) {
                log.error(err, 'Random bytes generation failed');

                return callback(err)
            }

            callback(null, buf.toString(byteToStringEncoding).slice(0, length))
        })
    },

    removeDup: (arr, lookFor) => {
        let unique = {}

        let list = []

        for(let i = 0; i < arr.length; i++)
            if(!unique[arr[i][lookFor]]) {
                unique[arr[i][lookFor]] = 1
                list.push(arr[i])
            }

        return list
    }
}
