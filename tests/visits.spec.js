const fs = require('fs')
const chai = require('chai')
const chaiHttp = require('chai-http')

const Visit = require('../models/visit')
const { Doctor } = require('../models/doctor')
const Patient = require('../models/patient')
const Medicine = require('../models/medicine')
const { prep, cleanDB } = require('./prep')
const val = require('../routes/validations').visit
const validateVisit = require('../routes/visits_handler').validateVisit
const validateMeds = require('../routes/visits_handler').validateMeds
const signin = require('./before_all').signin

const should = chai.should()
const expect = chai.expect

chai.use(chaiHttp)

describe('visits', () => {
    let server
    let cookie

    before((done) => {
        prep((svr) => {
            server = svr
            signin('santoshg550@gmail.com', 'Santosh@123', server, (ck) => {
                cookie = ck
                done()
            })
        })
    })

    describe('doctor id', (done) => {
        const right = [ 'abc' ]

        it(right[0] + ' is right', (done) => {
            val.docId(right[0]).should.equal(true)
            done()
        })

        const wrong = [ '', 8 ]

        wrong.forEach(function(id) {
            it(id + ' is wrong', (done) => {
                val.docId(id).should.equal(false)
                done()
            })
        })
    })

    describe('patient id', (done) => {
        const right = [ 8 ]

        it(right[0] + ' is right', (done) => {
            val.patId(right[0]).should.equal(true)
            done()
        })

        const wrong = [ '', 'abc' ]

        wrong.forEach(function(id) {
            it(id + ' is wrong', (done) => {
                val.patId(id).should.equal(false)
                done()
            })
        })
    })

    describe('doctor name', (done) => {
        const right = [ 'abc' ]

        it(right[0] + ' is right', (done) => {
            val.docName(right[0]).should.equal(true)
            done()
        })

        const wrong = [ '', 8 ]

        wrong.forEach(function(id) {
            it(id + ' is wrong', (done) => {
                val.docName(id).should.equal(false)
                done()
            })
        })
    })

    describe('reason', (done) => {
        const right = [ 'abc' ]

        it(right[0] + ' is right', (done) => {
            val.reason(right[0]).should.equal(true)
            done()
        })

        const wrong = [ '', 8 ]

        wrong.forEach((id) => {
            it(id + ' is wrong', (done) => {
                val.reason(id).should.equal(false)
                done()
            })
        })
    })

    describe('bill', (done) => {
        const right = [ 8, 0 ]

        right.forEach((bill) => {
            it(bill + ' is right', (done) => {
                expect(val.bill(bill)).to.be.true
                done()
            })
        })

        const wrong = [ '', 'abc' ]

        wrong.forEach((bill) => {
            it(bill + ' is wrong', (done) => {
                expect(val.bill(bill)).to.be.false
                done()
            })
        })
    })

    describe('paid', (done) => {
        const right = [ 8, 0 ]

        const bill = 10

        right.forEach((paid) => {
            it(paid + ' is right', (done) => {
                expect(val.paid(paid, 10)).to.be.true
                done()
            })
        })

        const wrong = [ '', 'abc', 23 ]

        wrong.forEach((paid) => {
            it(paid + ' is wrong', (done) => {
                expect(val.paid(paid, bill)).to.be.false
                done()
            })
        })
    })

    describe('prescription', (done) => {
        const right = [ 'abc' ]

        right.forEach((presc) => {
            it(presc + ' is right', (done) => {
                expect(val.presc(presc)).to.be.true
                done()
            })
        })

        const wrong = [ 8, '' ]

        wrong.forEach((presc) => {
            it(presc + ' is wrong', (done) => {
                expect(val.presc(presc)).to.be.false
                done()
            })
        })
    })

    describe('visit date', (done) => {
        const right = [ 'October 13, 2014 11:13:00' ]

        right.forEach((date) => {
            it(date + ' is right', (done) => {
                expect(val.visitDate(date)).to.be.true
                done()
            })
        })

        const wrong = [ 8, '', '9', 'abc' ]

        wrong.forEach((date) => {
            it(date + ' is wrong', (done) => {
                expect(val.visitDate(date)).to.be.false
                done()
            })
        })
    })

    describe('visit by', (done) => {
        describe('by date', () => {
            const right = [ 'October 13, 2014 11:13:00' ]

            right.forEach((date) => {
                it(date + ' is right', (done) => {
                    expect(val.view({
                        date: date
                    })).to.be.true
                    done()
                })
            })

            const wrong = [ 8, '', '9', 'abc', undefined ]

            wrong.forEach((date) => {
                it(date + ' is wrong', (done) => {
                    expect(val.view({
                        date: date
                    })).to.be.false
                    done()
                })
            })
        })

        describe('by from and to', () => {
            const right = [{
                from: 'October 13, 2014 11:13:00',
                to: 'October 14, 2014 11:13:00'
            }]

            right.forEach((date) => {
                it(date + ' is right', (done) => {
                    expect(val.view(date)).to.be.true
                    done()
                })
            })

            const wrongFrom = [
                {
                    from: '',
                    to: 'October 14, 2014 11:13:00'
                },
                {
                    from: 8,
                    to: 'October 14, 2014 11:13:00'
                },
                {
                    from: 'abc',
                    to: 'October 14, 2014 11:13:00'
                },
            ]

            wrongFrom.forEach((date) => {
                it('from ' + date.from + ' is wrong', (done) => {
                    expect(val.view(date)).to.be.false
                    done()
                })
            })

            const wrongTo = [
                {
                    from: 'October 14, 2014 11:13:00',
                    to: ''
                },
                {
                    from: 'October 14, 2014 11:13:00',
                    to: 8
                },
                {
                    from: 'October 14, 2014 11:13:00',
                    to: 'abc'
                }
            ]

            wrongTo.forEach((date) => {
                it('to ' + date.from + ' is wrong', (done) => {
                    expect(val.view(date)).to.be.false
                    done()
                })
            })
        })
    })

    describe('medicines', () => {
        const presc = 'lalalalla'

        it('medicines is array', (done) => {
            expect(val.medicines('abc', presc)).to.have.property('error')
            done()
        })

        const right = [ 'abc' ]

        right.forEach((med) => {
            it(med + ' is right', (done) => {
                expect(val.medicines([med], presc)).to.be.an('undefined')
                done()
            })
        })

        it('medicines without prescription is wrong', (done) => {
            expect(val.medicines([right[0]])).to.have.property('error')
            done()
        })

        const wrong = [ '', 8 ]

        wrong.forEach((med) => {
            it(med + ' is wrong', (done) => {
                expect(val.medicines([med], presc)).to.have.property('error')
                done()
            })
        })

        it('empty medicine array is wrong', (done) => {
            expect(val.medicines([], presc)).to.have.property('error')
            done()
        })

        it('medicine id is wrong', (done) => {
            const req = {
                body: {
                    medicines: [
                        'jkl'
                    ]
                }
            }

            const res = {
                error: (code, err) => {
                    expect(err).to.have.property('error')
                }
            }

            validateMeds(req, res, (err) => {
                expect(err).to.be.true
                done()
            })
        })

        it('medicine does not exist into the database', (done) => {
            const req = {
                body: {
                    medicines: [
                        '123456789012'
                    ]
                }
            }

            const res = {
                error: (code, err) => {
                    expect(code).to.equal(404)
                }
            }

            validateMeds(req, res, (err) => {
                expect(err).to.be.true
                done()
            })
        })

        it('medicine does exist into the database', (done) => {
            Medicine.find((err, docs) => {
                expect(err).to.be.null
                expect(docs.length).to.be.gt(0)

                const req = {
                    body: {
                        medicines: [
                            docs[0]._id.toString(),
                            docs[1]._id.toString()
                        ]
                    }
                }

                const res = {
                    error: (code, err) => {

                    }
                }

                validateMeds(req, res, (err) => {
                    expect(err).to.be.an('undefined')
                    done()
                })
            })
        })
    })

    let visit = {
        docId: 'abckd',
        patId: 'allalal',
        patName: 'naak',
        docName: 'sllslsl',
        reason: 'For checkup',
        bill: 89,
        paid: 88,
        visitDate: new Date,
        prescription: 'For something',
        prescImg: fs.readFileSync('tests/a_visitor.jpeg').toString('base64'),
        medicines: []
    }

    describe('validatation middleware', () => {
        ((visit) => {
            let invalidVal = {
                docId: '',
                patId: '',
                patName: '',
                docName: '',
                reason: '',
                bill: '',
                paid: '',
                visitDate: '',
                prescription: '',
                prescImg: ''
            }

            const visitorFields = Object.keys(visit)

            visitorFields.forEach((vf) => {
                it(invalidVal[vf] + ' is wrong ' + vf, (done) => {
                    let temp = visit[vf]

                    visit[vf] = invalidVal[vf]

                    chai
                        .request(server)
                        .post('/visits/add')
                        .set('Cookie', cookie)
                        .send(visit)
                        .end(function(err, res) {
                            expect(err).to.be.null
                            expect(res).to.have.status(400)

                            visit[vf] = temp

                            done()
                        })
                })
            })
        })(Object.assign({}, visit))
    })

    describe('add visit', () => {
        before(() => {
            Doctor.find((err, docs) => {
                expect(err).to.be.null
                expect(docs.length).to.not.equal(0)

                Patient.find((err, patients) => {
                    expect(err).to.be.null
                    expect(patients.length).to.not.equal(0)

                    Medicine.find((err, medicines) => {
                        expect(err).to.be.null
                        expect(medicines.length).to.not.equal(0)

                        visit.docId = docs[0]._id
                        visit.docName = docs[0].name

                        visit.patId = patients[0].id
                        visit.patName = patients[0].firstName + patients[0].lastName

                        visit.medicines = [ medicines[0].id ]
                    })
                })
            })
        })

        ;((visit) => {
            visit.reason = ''

            it('it should not add visit with invalid reason', (done) => {
                chai
                    .request(server)
                    .post('/visits/add')
                    .set('Cookie', cookie)
                    .send(visit)
                    .end(function(err, res) {
                        expect(err).to.be.null
                        expect(res).to.have.status(400)

                        done()
                    })
            })
        })(Object.assign({}, visit))

        it('it should add visit', (done) => {
            chai
                .request(server)
                .post('/visits/add')
                .set('Cookie', cookie)
                .send(visit)
                .end((err, res) => {
                    expect(err).to.be.null
                    expect(res).to.have.status(200)

                    done()
                })
        })

        ;((visit) => {
            it('it should not add visit with wrong medicines', (done) => {
                visit.medicines = [ '5a2' ]

                chai
                    .request(server)
                    .post('/visits/add')
                    .set('Cookie', cookie)
                    .send(visit)
                    .end((err, res) => {
                        expect(err).to.be.null
                        expect(res).to.have.status(400)

                        done()
                    })
            })
        })(Object.assign({}, visit))

        it('it should add visit without optional parts', (done) => {
            delete visit.prescription
            delete visit.prescImg
            delete visit.medicines

            visit.visitDate = new Date

            chai
                .request(server)
                .post('/visits/add')
                .set('Cookie', cookie)
                .send(visit)
                .end((err, res) => {
                    expect(err).to.be.null
                    expect(res).to.have.status(200)

                    done()
                })
        })

        it('it should not add dup visit', (done) => {
            chai
                .request(server)
                .post('/visits/add')
                .set('Cookie', cookie)
                .send(visit)
                .end((err, res) => {
                    expect(err).to.be.null
                    expect(res).to.have.status(409)

                    done()
                })
        })
    })

    describe('update visit', () => {
        let visit

        before((done) => {
            Visit.find({ 'medicines.0': { $exists: 1 }}, (err, docs) => {
                expect(err).to.be.null
                expect(docs.length).to.not.equal(0)

                visit = docs[0]

                done()
            })
        })

        it('it should update visit', (done) => {
            visit.reason = 'lafsljfslkjskjlsfjskfjsljskljdfsdfjffj'
            visit.medicines.push()

            chai
                .request(server)
                .post('/visits/update')
                .set('Cookie', cookie)
                .send(visit)
                .end((err, res) => {
                    expect(err).to.be.null
                    expect(res).to.have.status(200)

                    done()
                })
        })

        ;((visit) => {
            it('it should not update visit', (done) => {
                visit.reason = ''

                chai
                    .request(server)
                    .post('/visits/update')
                    .set('Cookie', cookie)
                    .send(visit)
                    .end((err, res) => {
                        expect(err).to.be.null
                        expect(res).to.have.status(400)

                        done()
                    })
            })
        })(Object.assign({}, visit))

        it('it should remove dup medicine', (done) => {
            const prevMedNum = visit.medicines.length

            visit.medicines.push(visit.medicines[0])

            chai
                .request(server)
                .post('/visits/update')
                .set('Cookie', cookie)
                .send(visit)
                .end((err, res) => {
                    expect(err).to.be.null
                    expect(res).to.have.status(200)

                    Visit.findOne({ visitDate: visit.visitDate }, (err, doc) => {
                        expect(err).to.be.null
                        expect(doc).to.be.not.null
                        expect(prevMedNum).to.equal(doc.medicines.length)

                        done()
                    })
                })
        })
    })

    describe('view visit', () => {
        let visits

        before((done) => {
            Visit.find((err, docs) => {
                expect(err).to.be.null
                expect(docs.length).to.not.equal(0)

                visits = docs

                done()
            })
        })

        it('it should fetch the visit by date', (done) => {
            const view = {
                date: visits[0].visitDate
            }

            chai
                .request(server)
                .post('/visits')
                .set('Cookie', cookie)
                .send(view)
                .end((err, res) => {
                    expect(err).to.be.null

                    expect(res).to.have.status(200)

                    expect(res.body).to.be.an('array')

                    if(res.body[0].medicines.length > 0)
                        expect(res.body[0].medicines[0]).to.have.property('name')

                    expect(res.body[0]._id.toString()).to.equal(visits[0]._id.toString())

                    done()
                })
        })

        it('it should fetch the visit by from to', (done) => {
            const view = {
                from: visits[0].visitDate,
                to: visits[visits.length - 1].visitDate
            }

            chai
                .request(server)
                .post('/visits')
                .set('Cookie', cookie)
                .send(view)
                .end((err, res) => {
                    expect(err).to.be.null

                    expect(res.body).to.be.an('array')
                    expect(res.body.length).to.equal(visits.length)
                    expect(res).to.have.status(200)

                    done()
                })
        })

        it('it should not fetch the visit with numbered from to', (done) => {
            const view = {
                from: 1,
                to: 3
            }

            chai
                .request(server)
                .post('/visits')
                .set('Cookie', cookie)
                .send(view)
                .end((err, res) => {
                    expect(err).to.be.null

                    expect(res).to.have.status(400)

                    done()
                })
        })

        it('it should fetch the visit by docId', (done) => {
            const view = {
                docId: visits[0].docId
            }

            chai
                .request(server)
                .post('/visits')
                .set('Cookie', cookie)
                .send(view)
                .end((err, res) => {
                    expect(err).to.be.null

                    expect(res.body).to.be.an('array')
                    expect(res.body[0].docId).to.equal(view.docId)
                    expect(res).to.have.status(200)

                    done()
                })
        })
    })
})
