const fs = require('fs')

const chai = require('chai')
const chaiHttp = require('chai-http')

const { Clinic, Token } = require('../models/clinic')
const { prep, cleanDB } = require('./prep')
const validations = require('../routes/validations')
const signin = require('./before_all').signin

const should = chai.should()
const expect = chai.expect

chai.use(chaiHttp)

describe('signin and signup', function() {
    let server
    let cookie

    before((done) => {
        prep((svr) => {
            server = svr
            done()
        })
    })

    describe('password', function(done) {
        const right = [ 'Santosh@123' ]

        right.forEach(function(password) {
            it(password + ' is right', function(done) {
                validations.clinic.isPasswordValid(password).should.equal(true)
                done()
            })
        })

        const wrong = [ 'Santosh' ]

        wrong.forEach(function(password) {
            it(password + ' is wrong', function(done) {
                validations.clinic.isPasswordValid(password).should.equal(false)
                done()
            })
        })
    })

    describe('mobile number', function(done) {
        const right = [ 9657681798 ]

        right.forEach(function(mob) {
            it(mob + ' is right', function(done) {
                validations.clinic.isMobValid(mob).should.equal(true)
                done()
            })
        })

        const wrong = [ '', 'abc', 23, 96576817988 ]

        wrong.forEach(function(mob) {
            it(mob + ' is wrong', function(done) {
                validations.clinic.isMobValid(mob).should.equal(false)
                done()
            })
        })
    })

    describe('email', function(done) {
        const right = ['bandichoderb@gmail.com']

        right.forEach(function(email) {
            it(email + ' is right', function(done) {
                validations.clinic.isEmailValid(email).should.equal(true)
                done()
            })
        })

        let wrong = [ '', 43, 'abc' ]

        wrong.forEach(function(email) {
            it(email + ' is wrong', function(done) {
                validations.clinic.isEmailValid(email).should.equal(false)
                done()
            })
        })
    })

    describe('name', function(done) {
        it('abc is right', function(done) {
            validations.clinic.isNameValid('abc').should.equal(true)
            done()
        })

        let wrong = [ '', 4 ]

        wrong.forEach(function(name) {
            it(name + ' is wrong', function(done) {
                validations.clinic.isNameValid(name).should.equal(false)
                done()
            })
        })
    })

    describe('clinic logo', function(done) {
        it('logo with size upto 50kb', function(done) {
            const logo = fs.readFileSync('tests/valid_logo.png').toString('base64')
            validations.clinic.isLogoValid(logo).should.equal(true)

            done()
        })

        let wrong = [ fs.readFileSync('tests/invalid_logo.png').toString('base64') ]

        wrong.forEach(function(logo) {
            it('logo is wrong', function(done) {
                validations.clinic.isLogoValid(logo).should.equal(false)

                done()
            })
        })
    })

    const clinic = {
        email: 'bandichoderb@gmail.com',
        name: 'Take care',
        password: 'Santosh@123',
        mobNo: 9657681798,
        address: {
            line_1: 'Ganganagar',
            line_2: 'Phursungi',
            country: 'India',
            city: 'Pune',
            state: 'Maharashtra',
            pincode: 412308
        },
        logo: fs.readFileSync('tests/valid_logo.png').toString('base64')
    }

    describe('signup', function() {
        ((clinic) => {
            const clinicFields = Object.keys(clinic)

            for(let i = 0; i < clinicFields.length; i++) {
                it(clinicFields[i] + ' is wrong', function(done) {
                    clinic[clinicFields[i]] = ''

                    chai
                        .request(server)
                        .post('/signup')
                        .send(clinic)
                        .end(function(err, res) {
                            expect(err).to.be.null

                            expect(res).to.have.status(400)

                            done()
                        })
                })
            }
        })(Object.assign({}, clinic))

        it('it should register the clinic without logo', function(done) {
            ((clinic) => {
                delete clinic.logo

                chai
                    .request(server)
                    .post('/signup')
                    .send(clinic)
                    .end(function(err, res) {
                        expect(err).to.be.null
                        res.should.have.status(200)
                        done()
                    })
            })(Object.assign({}, clinic))
        })

        it('it should register the clinic with logo', function(done) {
            ((clinic) => {
                Clinic.deleteMany({}, () => {
                    Token.deleteMany({}, () => {
                        chai
                            .request(server)
                            .post('/signup')
                            .send(clinic)
                            .end(function(err, res) {
                                expect(err).to.be.null
                                res.should.have.status(200)
                                done()
                            })
                    })
                })
            })(Object.assign({}, clinic))
        })

        it('it should not register the clinic with dup email id', function(done) {
            chai
                .request(server)
                .post('/signup')
                .send(clinic)
                .end(function(err, res) {
                    expect(err).to.be.null
                    res.should.have.status(400)
                    done()
                })
        })

        it('it should not register the clinic with dup mobile number', function(done) {
            ((clinic) => {
                clinic.email = 'abc@gmail.com'

                chai
                    .request(server)
                    .post('/signup')
                    .send(clinic)
                    .end(function(err, res) {
                        expect(err).to.be.null
                        res.should.have.status(400)

                        done()
                    })
            })(Object.assign({}, clinic))
        })

        it('it should not register the clinic with large logo', function(done) {
            ((clinic) => {
                clinic.logo = fs.readFileSync('tests/invalid_logo.png').toString('base64')

                chai
                    .request(server)
                    .post('/signup')
                    .send(clinic)
                    .end(function(err, res) {
                        expect(err).to.be.null
                        res.should.have.status(400)

                        done()
                    })
            })(Object.assign({}, clinic))
        })

        describe('verify registration', function() {
            it('verify resend', function(done) {
                chai
                    .request(server)
                    .post('/resend')
                    .send({
                        email: 'bandichoderb@gmail.com'
                    })
                    .end(function(err, res) {
                        expect(err).to.be.null
                        res.should.have.status(200)
                        done()
                    })
            })

            it('verify signup', function(done) {
                Token.findOne({ userId: 'bandichoderb@gmail.com' }, function(err, token) {
                    expect(err).to.be.null
                    expect(token).to.be.not.null

                    chai
                        .request(server)
                        .get('/verify')
                        .query({
                            email: 'bandichoderb@gmail.com',
                            token: token.token
                        })
                        .end(function(err, res) {
                            expect(err).to.be.null
                            res.should.have.status(200)
                            done()
                        })
                })
            })
        })
    })

    describe('signin', function() {
        it('it should signin successfully', function(done) {
            signin('bandichoderb@gmail.com', 'Santosh@123', server, (ck) => {
                expect(ck).to.be.a('string')
                cookie = ck
                done()
            })
        })
    })

    describe('client info', function() {
        it('it should fetch record', function(done) {
                chai
                    .request(server)
                    .get('/client')
                    .set('Cookie', cookie)
                    .end(function(err, res) {
                        expect(err).to.be.null

                        expect(res).to.have.status(200)

                        done()
                    })
        })
    })

    describe('update registration', function() {
        it('it should update registration', function(done) {
            clinic.email = 'santoshg550@gmail.com'
            clinic.name = 'Please take care'
            clinic.mobNo = 9404609084
            clinic.address.line_1 = 'Ganga'

            chai
                .request(server)
                .post('/client/update')
                .set('Cookie', cookie)
                .send(clinic)
                .end(function(err, res) {
                    expect(err).to.be.null

                    res.should.have.status(200)

                    done()
                })
        })

        it('verify updated email', function(done) {
            Token.findOne({ userId: 'santoshg550@gmail.com' }, function(err, token) {
                expect(err).to.be.null
                expect(token).to.be.not.null

                chai
                    .request(server)
                    .get('/verify')
                    .query({
                        email: 'santoshg550@gmail.com',
                        token: token.token
                    })
                    .end(function(err, res) {
                        expect(err).to.be.null
                        res.should.have.status(200)
                        done()
                    })
            })
        })
    })

    describe('reject update', function() {
        ;((clinic) => {
            const updateFields = Object.keys(clinic)

            const invVal = ''

            for(let i = 0; i < updateFields.length; i++)
                it(invVal + ' is wrong ' + updateFields[i], function(done) {
                    clinic[updateFields[i]] = invVal 

                    chai
                        .request(server)
                        .post('/client/update')
                        .set('Cookie', cookie)
                        .send(clinic)
                        .end(function(err, res) {
                            expect(err).to.be.null
                            res.should.have.status(400)

                            done()
                        })
                })
        })(clinic)
    })

    describe('reset', () => {
        it('forgot password', function(done) {
            chai
                .request(server)
                .post('/forgot')
                .send({
                    email: 'santoshg550@gmail.com'
                })
                .end((err, res) => {
                    expect(err).to.be.null
                    res.should.have.status(200)

                    done()
                })
        })
    })
})
