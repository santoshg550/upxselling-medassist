[
    'DATABASE_URI',
    'SESSION_SECRET',
    'EMAIL',
    'PASSWORD',
    'ITERATIONS',
    'KEY_LENGTH',
    'DIGEST',
    'SALT_LENGTH',
    'BYTE_TO_STRING_ENCODING',
    'RESET_TOKEN_LENGTH',
    'RESET_TOKEN_EXPIRES'
].forEach((name) => {
    if(!process.env[name])
        throw new Error(`Environment variable ${name} is missing`)
})

module.exports = {
    databaseURI: process.env.DATABASE_URI,
    sessionSecret: process.env.SESSION_SECRET,
    port: process.env.PORT,
    iterations: Number(process.env.ITERATIONS),
    keyLength: Number(process.env.KEY_LENGTH),
    saltLength: Number(process.env.SALT_LENGTH),
    digest: process.env.DIGEST,
    byteToStringEncoding: process.env.BYTE_TO_STRING_ENCODING,
    resetTokenLength: Number(process.env.RESET_TOKEN_LENGTH),
    resetTokenExpires: Number(process.env.RESET_TOKEN_EXPIRES),
    email: {
        id: process.env.EMAIL,
        password: process.env.PASSWORD
    }
}
